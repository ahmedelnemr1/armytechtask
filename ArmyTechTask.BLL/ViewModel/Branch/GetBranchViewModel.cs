﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmyTechTask.BLL.ViewModel.Cashier;
using ArmyTechTask.BLL.ViewModel.City;
using ArmyTechTask.BLL.ViewModel.Invoice;

namespace ArmyTechTask.BLL.ViewModel.Branch
{
   public class GetBranchViewModel
    {
        public int ID { get; set; }

        public string BranchName { get; set; }

        public int CityID { get; set; }

        public virtual GetCityViewModel City { get; set; }

        public virtual ICollection<GetCashierViewModel> Cashiers { get; set; }

      
        public virtual ICollection<GetInvoiceViewModel> InvoiceHeaders { get; set; }
    }
}
