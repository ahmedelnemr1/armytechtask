﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmyTechTask.BLL.ViewModel.Branch;
using ArmyTechTask.BLL.ViewModel.Invoice;

namespace ArmyTechTask.BLL.ViewModel.Cashier
{
   public class GetCashierViewModel
    {
        public int ID { get; set; }

        
        public string CashierName { get; set; }

        public int BranchID { get; set; }

        public virtual GetBranchViewModel Branch { get; set; }

        
        public virtual ICollection<GetInvoiceViewModel> InvoiceHeaders { get; set; }
    }
}
