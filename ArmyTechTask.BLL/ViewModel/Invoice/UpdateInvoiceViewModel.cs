﻿using System;
using System.ComponentModel.DataAnnotations;
using ArmyTechTask.BLL.ViewModel.Branch;
using ArmyTechTask.BLL.ViewModel.Cashier;

namespace ArmyTechTask.BLL.ViewModel.Invoice
{
   public class UpdateInvoiceViewModel
    {
        [Required(ErrorMessage = "Invoice Header Id is Required")]
        public long ID { get; set; }

        public string CustomerName { get; set; }

        public DateTime Invoicedate { get; set; }

        public int BranchID { get; set; }

        public virtual GetBranchViewModel Branch { get; set; }

        public int? CashierID { get; set; }
        public virtual GetCashierViewModel Cashier { get; set; }

    }
}
