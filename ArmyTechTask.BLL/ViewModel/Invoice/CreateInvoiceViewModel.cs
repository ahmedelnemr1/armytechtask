﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ArmyTechTask.BLL.ViewModel.Branch;
using ArmyTechTask.BLL.ViewModel.Cashier;
using ArmyTechTask.BLL.ViewModel.InvoiceDetails;

namespace ArmyTechTask.BLL.ViewModel.Invoice
{
   public class CreateInvoiceViewModel
    {
        [Required(ErrorMessage = "Customer Name Is Required")]
        public string CustomerName { get; set; }

        public DateTime Invoicedate { get; set; }

        public int BranchID { get; set; }

        public virtual GetBranchViewModel Branch { get; set; }

        public int? CashierID { get; set; }
        public virtual GetCashierViewModel Cashier { get; set; }

        public virtual ICollection<GetInvoiceDetailsViewModel> InvoiceDetails { get; set; }
    }
}
