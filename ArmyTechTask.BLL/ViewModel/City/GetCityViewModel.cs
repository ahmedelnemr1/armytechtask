﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmyTechTask.BLL.ViewModel.Branch;

namespace ArmyTechTask.BLL.ViewModel.City
{
   public class GetCityViewModel
    {
        public int ID { get; set; }

        public string CityName { get; set; }

        public virtual ICollection<GetBranchViewModel> Branches { get; set; }
    }
}
