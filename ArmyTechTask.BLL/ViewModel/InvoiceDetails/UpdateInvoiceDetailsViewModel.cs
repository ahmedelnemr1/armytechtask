﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmyTechTask.BLL.ViewModel.Cashier;
using ArmyTechTask.BLL.ViewModel.City;
using ArmyTechTask.BLL.ViewModel.Invoice;

namespace ArmyTechTask.BLL.ViewModel.InvoiceDetails
{
   public class UpdateInvoiceDetailsViewModel
    {
        [Required(ErrorMessage = "Invoice Details Id Is Required")]
        public long ID { get; set; }


        public string ItemName { get; set; }

        public double ItemCount { get; set; }

        public double ItemPrice { get; set; }

        public long InvoiceHeaderID { get; set; }
        public virtual GetInvoiceViewModel InvoiceHeader { get; set; }


    }
}
