﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArmyTechTask.BLL.Services.Invoice;
using ArmyTechTask.BLL.Services.InvoiceDetails;
using ArmyTechTask.DAL.Database;

namespace ArmyTechTask.Controllers
{
    public class InvoiceController : Controller
    {
        private readonly IInvoiceDetailService _invoiceDetailService;
        private readonly IInvoiceService _invoiceService;

        public InvoiceController()
        {
            var dbContext = new ArmyTechTaskDbContext();
            _invoiceDetailService = new InvoiceDetailService(dbContext);
            _invoiceService = new InvoiceService(dbContext);
        }
        // GET: Invoice
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult InvoiceList()
        {
            var invoices = _invoiceService.GetInvoices().ToList();
            return PartialView(invoices);
        }

    }
}